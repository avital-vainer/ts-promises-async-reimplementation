
//--------------------------------------------------

export async function race(promises: any[] | Promise<any> []): Promise<any> {
    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            resolve(await p);
        });
    });
}
//--------------------------------------------------

export async function some(promises: any[] | Promise<any> [], num: number): Promise<any[]> {
    const results: any[] = [];
    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            results.push(await p);
            if (results.length === num) resolve(results);
        });
    });
}
