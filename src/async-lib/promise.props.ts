
interface IPromisesObj {
    [key: string]: any | Promise<any>;
}

export async function props(promisesObj: IPromisesObj) {
    const results: any = {};
    for (const key in promisesObj) {
        results[key] = await promisesObj[key];
    }
    return results;
}