//--------------------------------------------------

import { callback } from "./promise.utils.js";

export async function each(iterable: any[] | Promise<any>[], cb: callback): Promise<any[]> {
    for (const [i, item] of iterable.entries()) {
        await cb(await item, i, iterable.length);
    }
    return iterable;
}

//--------------------------------------------------
