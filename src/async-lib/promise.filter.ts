//--------------------------------------------------

export async function filterSeries(iterable: string | any[] | Promise<any>[], cb: (val: any) => boolean | Promise<boolean>): Promise<string | any[]> {
    const results = [];
    for (const item of iterable) {
        if ((await cb(item)) === true) results.push(item);
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------

export async function filterParallel(iterable: string | any[] | Promise<any>[], cb: (val: any) => boolean | Promise<boolean>): Promise<string | any[]> {
    const results = [];
    const pending = Array.from(iterable, async (item) => cb(await item));

    for (const [i, p] of pending.entries()) {
        if ((await p) === true) results.push(iterable[i]);
    }

    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------
