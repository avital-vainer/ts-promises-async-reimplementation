//--------------------------------------------------

import { callback } from "./promise.utils.js";

export async function mapParallel(iterable: string | any[] | Promise<any>[], cb: callback): Promise<string | any[]> {
    const results = [];
    const pending = Array.from(iterable, async (item) => cb(await item));
    for (const p of pending) {
        results.push(await p);
    }
    return results;
}

//--------------------------------------------------

export async function mapSeries(iterable: string | any[] | Promise<any>[], cb: callback): Promise<string | any[]> {
    const results = [];
    for (const item of iterable) {
        results.push(await cb(item));
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------
