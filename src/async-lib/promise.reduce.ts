

export async function reduce(iterable: any[] | Promise<any> [], cb: (acc: any, item: any) => any, initial?: any) {
    let aggregator = initial === undefined ? iterable[0] : initial;

    for (const item of iterable) {
        aggregator = await cb(aggregator, await item);
    }

    return aggregator;
}