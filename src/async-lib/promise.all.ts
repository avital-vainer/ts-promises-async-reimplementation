export async function all(promises: any[] | Promise<any>[]): Promise<any[]> {
    try {
        const results = [];
        for (const p of promises) {
            results.push(await p);
        }
        return results;
    } catch (error) {
        console.log(error);
        throw error;
    }
}
