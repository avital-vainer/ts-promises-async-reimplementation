import log from "@ajar/marker";

export type callback = (item: Promise<any>, index?: number, arrayLength?: number) => Promise<any>;

export const delay = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

//--------------------------------------------------

export const echo = async (msg: string, ms: number) => {
    // log.yellow(`--> start ${msg}`);
    await delay(ms);
    // log.blue(`finish <-- ${msg}`);
    return msg;
};

//--------------------------------------------------

export const random = (max: number, min = 0) =>
    min + Math.round(Math.random() * (max - min));
