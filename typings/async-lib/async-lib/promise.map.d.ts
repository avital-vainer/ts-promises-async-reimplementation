import { callback } from "./promise.utils.js";
export declare function mapParallel(iterable: string | any[] | Promise<any>[], cb: callback): Promise<string | any[]>;
export declare function mapSeries(iterable: string | any[] | Promise<any>[], cb: callback): Promise<string | any[]>;
