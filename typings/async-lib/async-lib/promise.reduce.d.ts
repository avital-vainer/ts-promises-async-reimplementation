export declare function reduce(iterable: any[] | Promise<any>[], cb: (acc: any, item: any) => any, initial?: any): Promise<any>;
