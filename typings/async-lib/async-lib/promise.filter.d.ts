export declare function filterSeries(iterable: string | any[] | Promise<any>[], cb: (val: any) => boolean | Promise<boolean>): Promise<string | any[]>;
export declare function filterParallel(iterable: string | any[] | Promise<any>[], cb: (val: any) => boolean | Promise<boolean>): Promise<string | any[]>;
