import { callback } from "./promise.utils.js";
export declare function each(iterable: any[] | Promise<any>[], cb: callback): Promise<any[]>;
