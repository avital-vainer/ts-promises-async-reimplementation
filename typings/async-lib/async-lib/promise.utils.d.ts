export declare type callback = (item: Promise<any>, index?: number, arrayLength?: number) => Promise<any>;
export declare const delay: (ms: number) => Promise<unknown>;
export declare const echo: (msg: string, ms: number) => Promise<string>;
export declare const random: (max: number, min?: number) => number;
