interface IPromisesObj {
    [key: string]: any | Promise<any>;
}
export declare function props(promisesObj: IPromisesObj): Promise<any>;
export {};
