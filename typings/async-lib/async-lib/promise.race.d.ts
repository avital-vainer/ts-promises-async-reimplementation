export declare function race(promises: any[] | Promise<any>[]): Promise<any>;
export declare function some(promises: any[] | Promise<any>[], num: number): Promise<any[]>;
