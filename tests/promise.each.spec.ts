import { expect } from "chai";
import { each } from "../src/async-lib/promise.each";
import { delay } from "../src/async-lib/promise.utils";

describe("promise.each", () => {
    it("should exist", () => {
        expect(each).to.be.a("function");
        expect(each).to.be.instanceOf(Function);
    });

    it("should execute a callback on each item and return the original iterable", async () => {
        const items = [0, 1, Promise.resolve(2), 3, 4, 5, Promise.resolve(6)];

        // const res = await each(items, (item) => {
        //     return delay(100).then( () =>
        //         console.log(item)
        //     );
        // });

        const res = await each(items, (item) => {
            if(typeof item === "number") return delay(100);
            else return delay(200);
        });
        expect(res).to.be.eql(items);
    });
});
