import { expect } from "chai";
import { filterSeries, filterParallel } from "../src/async-lib/promise.filter";

describe("promise.filter", () => {

    context("#series", () => {
        
        it("should exist", () => {
            expect(filterSeries).to.be.a("function");
            expect(filterSeries).to.be.instanceOf(Function);
        });

        it("should return a filtered array", async() => {
            const items = [1, 2, 3, 4, Promise.resolve(5), 51, 17, 33, 21, -7];
            const filtered = await filterSeries(items, async (x) => (await x) > 3);
            
            expect(filtered).to.eql([4, items[4], 51, 17, 33, 21]);  // items[4] --> the Promise.resolve(5) object from the original array
        });

        it("should return a filtered array 2", async() => {
            const items = [1, 2, 3, "bye", 4, Promise.resolve(5), 51, 17, "hello", 33, 21, -7];
            const filtered = await filterSeries(items, async (val) => {
                return typeof (await val) === "number";
             });
            expect(filtered).to.eql([1, 2, 3, 4, items[5], 51, 17, 33, 21, -7]);
        });

    });


    context("#parallel", () => {

        it("should exist", () => {
            expect(filterParallel).to.be.a("function");
            expect(filterParallel).to.be.instanceOf(Function);
        });

        it("should return a filtered array", async () => {
            const items = [1, 2, 3, "bye", 4, Promise.resolve(5), 51, 17, "hello", 33, 21, -7];
            const filtered = await filterParallel(items, async (x) => (await x) > 10);

            expect(filtered).to.eql([51, 17, 33, 21]);
        });
    });

});