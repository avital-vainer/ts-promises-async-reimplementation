import { expect } from "chai";
import { all } from "../src/async-lib/promise.all";

describe("promise.all", () => {

    it("should exist", () => {
        expect(all).to.be.a("function");
        expect(all).to.be.instanceOf(Function);
    });

    it("should return an array of the resolved values", async () => {
        const promise1 = Promise.resolve(3);
        const promise2 = 42;
        const promise3 = new Promise((resolve) => {
            setTimeout(resolve, 100, "foo");
        });

        const res = await all([promise1, promise2, promise3]);

        expect(res).to.be.eql([3, 42, "foo"]);
    });

});