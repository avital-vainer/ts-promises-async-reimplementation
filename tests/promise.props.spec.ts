import { expect } from "chai";
import { props } from "../src/async-lib/promise.props";

describe("promise.props", () => {
    
    it("should exist", () => {
        expect(props).to.be.a("function");
        expect(props).to.be.instanceOf(Function);
    });

    it("should return an object with the resolved values", async () => {
        const promise1 = Promise.resolve(3);
        const promise2 = 42;
        const promise3 = new Promise((resolve) => {
            setTimeout(resolve, 100, "foo");
        });

        const res = await props({p1: promise1,p2: promise2, p3: promise3});

        expect(res).to.deep.equal({ p1: 3, p2: 42, p3: "foo"});
    });
});
