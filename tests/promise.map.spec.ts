import { expect } from "chai";
import { mapSeries, mapParallel } from "../src/async-lib/promise.map";


describe("promise.map", () => {

    context("#series", () => {

        it("should exist", () => {
            expect(mapSeries).to.be.a("function");
            expect(mapSeries).to.be.instanceOf(Function);
        });

        it("should return a new array with the modified values", async () => {
            const items = [0, 1, Promise.resolve(2), 3, 4, 5, Promise.resolve(6), 7, 8, 9];

            const modified = await mapSeries(items, async (item) => {
                return (await item) + 2;
            });

            expect(modified).to.eql([2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);
        });

    });

    context("#parallel", () => {

        it("should exist", () => {
            expect(mapParallel).to.be.a("function");
            expect(mapParallel).to.be.instanceOf(Function);
        });

        it("should return a new array with the modified values", async () => {
            const items = [0, 1, Promise.resolve(2), 3, 4, 5, Promise.resolve(6), 7, 8, 9];

            const modified = await mapParallel(items, async (item) => {
                return (await item) * 2;
            });

            expect(modified).to.eql([0, 2, 4, 6, 8, 10, 12, 14, 16, 18]);
        });

    });
});