import { expect } from "chai";
import { race, some } from "../src/async-lib/promise.race";
import { echo } from "../src/async-lib/promise.utils";


describe("promise.race", () => {

    context("#race", () => {

        it("should exist", () => {
            expect(race).to.be.a("function");
            expect(race).to.be.instanceOf(Function);
        });

        it("should return when the first promise resolves, with the resolved value", async () => {
            const p1 = echo("3", 1500);
            const p2 = echo("5", 1000);
            const p3 = echo("1", 2000);
            const pending = [ p1, p2, p3, 20];

            const res = await race(pending);
            expect(res).to.equal(20);
        });

    });


    context("#some", () => {

        it("should exist", () => {
            expect(some).to.be.a("function");
            expect(some).to.be.instanceOf(Function);
        });

        it("should return when num promises resolve, with their resolved values", async () => {
            const p1 = echo("3", 1500);
            const p2 = echo("5", 1000);
            const p3 = echo("1", 2000);
            const pending = [ p1, p2, p3, 20];

            const res = await some(pending, 3);
            expect(res).to.eql([20, "5", "3"]);
        });

    });
});