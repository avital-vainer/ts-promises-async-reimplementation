import { expect } from "chai";
import { reduce } from "../src/async-lib/promise.reduce";


describe("promise.reduce", () => {

    it("should exist", () => {
        expect(reduce).to.be.a("function");
        expect(reduce).to.be.instanceOf(Function);
    });

    it("should return the sum of the array", async () => {
        const arr = [1, 2, 3, 4, Promise.resolve(5)];

        const sum = await reduce(arr, (total, item) => total + item, 0);
        expect(sum).to.equal(15);
    });
});